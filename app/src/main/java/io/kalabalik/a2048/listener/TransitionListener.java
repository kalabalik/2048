package io.kalabalik.a2048.listener;

import android.transition.Transition;

import io.kalabalik.a2048.MainActivity;

public class TransitionListener implements Transition.TransitionListener {
    private final MainActivity mainActivity;

    public TransitionListener(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    @Override
    public void onTransitionStart(Transition transition) {
    }

    @Override
    public void onTransitionEnd(Transition transition) {
        mainActivity.onTransitionEnd();
    }

    @Override
    public void onTransitionCancel(Transition transition) {
    }

    @Override
    public void onTransitionPause(Transition transition) {
    }

    @Override
    public void onTransitionResume(Transition transition) {
    }
}