package io.kalabalik.a2048.model;


import android.os.Handler;

import io.kalabalik.a2048.MainActivity;

@SuppressWarnings("ManualArrayCopy")
public class Model {
    private final MainActivity mainActivity;
    private final Handler handler = new Handler();
    private final int delay;

    private final Tile[] tiles;

    public enum Direction {
        UP, END, DOWN, START;
    }

    private boolean dirty = true;

    public Model(MainActivity mainActivity, Tile[] tiles, int delay) {
        this.mainActivity = mainActivity;
        this.tiles = tiles;
        this.delay = delay;
        setRandomNewTile();
    }

    public void modifyModel(Direction direction) {
        for (int outer = 0; outer < 4; outer++) {
            reconstructArray(direction, manipulateQuadruple(deconstructArray(direction, outer)), outer);
        }
    }

    private Tile[] deconstructArray(Direction direction, int outer) {
        Tile dummy = new Tile(0, 0);
        Tile[] quadruple = {dummy, dummy, dummy, dummy};
        switch (direction) {
            case START:
                for (int inner = 0; inner < 4; inner++) {
                    quadruple[inner] = tiles[inner + outer * 4];
                }
                break;
            case UP:
                for (int inner = 0; inner < 4; inner++) {
                    quadruple[inner] = tiles[outer + inner * 4];
                }
                break;
            case END:
                for (int inner = 3; inner >= 0; inner--) {
                    quadruple[3 - inner] = tiles[inner + outer * 4];
                }
                break;
            case DOWN:
                for (int inner = 3; inner >= 0; inner--) {
                    quadruple[3 - inner] = tiles[outer + inner * 4];
                }
                break;
        }
        return quadruple;
    }

    private Tile[] manipulateQuadruple(Tile[] quadruple) {
        // move non-zeros to the left
        boolean nonZeroFound = false;
        for (int i = 3; i >= 0; i--) {
            if (quadruple[i].getValue() == 0) {
                if (nonZeroFound) {
                    rotateQuadruple(quadruple, i);
                }
            } else {
                nonZeroFound = true;
            }
        }
        // merge equal non-zeros
        for (int i = 0; i < 3; i++) {
            if (quadruple[i].getValue() != 0 && quadruple[i].getValue() == quadruple[i + 1].getValue()) {
                rotateQuadruple(quadruple, i);
                quadruple[i].setValue(2 * quadruple[i].getValue());
            }
        }
        return quadruple;
    }

    private void rotateQuadruple(Tile[] quadruple, int index) {
        dirty = true;
        int id = quadruple[index].getId();
        for (int i = index; i < 3; i++) {
            quadruple[i] = quadruple[i + 1];
        }
        quadruple[3] = new Tile(id, 0);
    }

    private void reconstructArray(Direction direction, Tile[] quadruple, final int outer) {
        switch (direction) {
            case START:
                for (int inner = 0; inner < 4; inner++) {
                    if (tiles[inner + outer * 4] != quadruple[inner]) {
                        tiles[inner + outer * 4] = quadruple[inner];
                        if (inner == 3) {
                            final int innerFinal = inner;
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mainActivity.refreshTile(innerFinal + outer * 4);
                                }
                            }, delay);
                        } else {
                            mainActivity.refreshTile(inner + outer * 4);
                        }
                    }
                }
                break;
            case UP:
                for (int inner = 0; inner < 4; inner++) {
                    if (tiles[outer + inner * 4] != quadruple[inner]) {
                        tiles[outer + inner * 4] = quadruple[inner];
                        if (inner == 3) {
                            final int innerFinal = inner;
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mainActivity.refreshTile(outer + innerFinal * 4);
                                }
                            }, delay);
                        } else {
                            mainActivity.refreshTile(outer + inner * 4);
                        }
                    }
                }
                break;
            case END:
                for (int inner = 3; inner >= 0; inner--) {
                    if (tiles[inner + outer * 4] != quadruple[3 - inner]) {
                        tiles[inner + outer * 4] = quadruple[3 - inner];
                        if (inner == 0) {
                            final int innerFinal = inner;
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mainActivity.refreshTile(innerFinal + outer * 4);
                                }
                            }, delay);
                        } else {
                            mainActivity.refreshTile(inner + outer * 4);
                        }
                    }
                }
                break;
            case DOWN:
                for (int inner = 3; inner >= 0; inner--) {
                    if (tiles[outer + inner * 4] != quadruple[3 - inner]) {
                        tiles[outer + inner * 4] = quadruple[3 - inner];
                        if (inner == 0) {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mainActivity.refreshTile(outer);
                                }
                            }, delay);
                        } else {
                            mainActivity.refreshTile(outer + inner * 4);
                        }
                    }
                }
                break;
        }
    }

    public int setRandomNewTile() {
        // TODO reorder states
        // TODO avoid long searches towards the end of available space
        int index;
        if (dirty) {
            dirty = false;
            do {
                index = (int) (Math.random() * 16);
            } while (tiles[index].getValue() != 0);
            int value = Math.random() < .9 ? 2 : 4;
            tiles[index].setValue(value);
        } else {
            index = -2;
            boolean firstEmptyFound = false;
            for (int i = 0; i < 16; i++) {
                if (tiles[i].getValue() == 0) {
                    if (firstEmptyFound) {
                        index = -1;
                    }
                    firstEmptyFound = true;
                } else if (tiles[i].getValue() == 2048) {
                    index = -3;
                }
            }
        }
        return index;
    }

    public Tile[] getTiles() {
        return tiles;
    }
}