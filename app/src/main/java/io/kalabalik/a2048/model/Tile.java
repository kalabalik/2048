package io.kalabalik.a2048.model;

public class Tile {
    private int value;
    private int id;

    public Tile(int id, int value) {
        this.id = id;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tile getTile() {
        return this;
    }

    public void setTile(Tile tile) {
        setValue(tile.value);
        setId(tile.id);
    }
}