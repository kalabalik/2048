package io.kalabalik.a2048.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import io.kalabalik.a2048.MainActivity;
import io.kalabalik.a2048.R;

public class FailureDialog extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final MainActivity activity = (MainActivity) getActivity();
        ContextThemeWrapper ctw = new ContextThemeWrapper(activity, R.style.DialogTheme);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctw);
        builder.setMessage(R.string.dialog_failure)
                .setPositiveButton(R.string.reset, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (activity != null) {
                            Intent intent = activity.getIntent();
                            activity.finish();
                            startActivity(intent);
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (activity != null) {
                            activity.finish();
                        }
                    }
                });
        return builder.create();
    }
}
