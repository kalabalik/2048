package io.kalabalik.a2048;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.transition.AutoTransition;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.TypedValue;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintSet;

import io.kalabalik.a2048.dialog.FailureDialog;
import io.kalabalik.a2048.dialog.SuccessDialog;
import io.kalabalik.a2048.layout.CustomConstraintLayout;
import io.kalabalik.a2048.listener.OnSwipeTouchListener;
import io.kalabalik.a2048.listener.TransitionListener;
import io.kalabalik.a2048.model.Model;
import io.kalabalik.a2048.model.Tile;

public class MainActivity extends AppCompatActivity {
    private Tile[] tiles;
    private final static String IDS = "ids";
    private final static String VALUES = "values";

    private Model model;
    private CustomConstraintLayout constraintLayout;
    private Transition transition;
    private TransitionListener transitionListener;

    private String packageName;
    private Resources res;
    private final Handler handler = new Handler();
    private final int delay = 100;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.main_activity);

        packageName = getPackageName();
        res = getResources();

        tiles = new Tile[16];
        if (savedInstanceState == null) {
            for (int i = 0; i < tiles.length; i++) {
                int tvId = findViewById(res.getIdentifier("tv" + i, "id", packageName)).getId();
                tiles[i] = new Tile(tvId, 0);
            }
        } else {
            int[] ids = savedInstanceState.getIntArray(IDS);
            int[] values = savedInstanceState.getIntArray(VALUES);
            if (ids != null && values != null) {
                for (int i = 0; i < tiles.length; i++) {
                    tiles[i] = new Tile(ids[i], values[i]);
                }
            }
        }

        model = new Model(this, tiles, delay);
        constraintLayout = findViewById(R.id.innerlayout);

        refreshUI();
    }

    @Override
    protected void onPause() {
        super.onPause();
        constraintLayout.setOnTouchListener(null);
        transition.removeListener(transitionListener);
        transition = null;
        transitionListener = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        constraintLayout.setOnTouchListener(new OnSwipeTouchListener(this, model));
        transition = new AutoTransition();
        transition.setDuration(delay);
        transitionListener = new TransitionListener(this);
        transition.addListener(transitionListener);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        int[] ids = new int[tiles.length];
        int[] values = new int[tiles.length];
        for (int i = 0; i < tiles.length; i++) {
            ids[i] = tiles[i].getId();
            values[i] = tiles[i].getValue();
        }
        savedInstanceState.putIntArray(IDS, ids);
        savedInstanceState.putIntArray(VALUES, values);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        packageName = null;
        res = null;
        model = null;
        constraintLayout = null;
    }

    public void refreshUI() {
        // connects all tiles to the grid
        for (int i = 0; i < 16; i++) {
            refreshTile(i);
        }
    }

    public void refreshTile(final int index) {
        final int tvID = model.getTiles()[index].getId();

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(constraintLayout);

        int connector = res.getIdentifier("hg" + index / 4 * 25, "id", packageName);
        constraintSet.connect(tvID, ConstraintSet.TOP, connector, ConstraintSet.BOTTOM, 15);

        connector = res.getIdentifier("hg" + (index / 4 + 1) * 25, "id", packageName);
        constraintSet.connect(tvID, ConstraintSet.BOTTOM, connector, ConstraintSet.TOP, 15);

        connector = res.getIdentifier("vg" + index % 4 * 25, "id", packageName);
        constraintSet.connect(tvID, ConstraintSet.START, connector, ConstraintSet.END, 15);

        connector = res.getIdentifier("vg" + (index % 4 + 1) * 25, "id", packageName);
        constraintSet.connect(tvID, ConstraintSet.END, connector, ConstraintSet.START, 15);

        TransitionManager.beginDelayedTransition(constraintLayout, transition);
        constraintSet.applyTo(constraintLayout);

        drawText(tvID, index);
    }

    void drawText(int tvID, int index) {
        final TextView tv = findViewById(tvID);
        final int value = model.getTiles()[index].getValue();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv.setText(value == 0 ? "" : String.valueOf(value));
            }
        }, delay);
        int size = value < 100 ? 48 : value < 1000 ? 36 : 24;
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, size);
        int textColor = value == 0 ? res.getColor(R.color.transparent) : res.getColor(R.color.number);
        tv.setTextColor(textColor);
        int color = value == 0 ? res.getColor(R.color.transparent) : res.getColor(R.color.tile);
        tv.setBackgroundColor(color);
    }

    public void onTransitionEnd() {
        final int newTileIndex = model.setRandomNewTile();
        if (newTileIndex == -3) {
            SuccessDialog dialog = new SuccessDialog();
            dialog.setCancelable(true);
            dialog.show(getSupportFragmentManager(), "success");
        } else if (newTileIndex == -2) {
            constraintLayout.setOnTouchListener(null);
            transition.removeListener(transitionListener);
            FailureDialog dialog = new FailureDialog();
            dialog.setCancelable(false);
            dialog.show(getSupportFragmentManager(), "failure");
        } else if (newTileIndex != -1) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int tvID = model.getTiles()[newTileIndex].getId();
                    drawText(tvID, newTileIndex);
                }
            }, delay);
        }
    }
}